module.exports = {
  setHomeActivity: function(state, success_callback, error_callback) {
    cordova.exec(success_callback, error_callback, "kioskmanager", "setHomeActivity", [state]);
  },
  setPinnedMode: function(state, success_callback, error_callback) {
    cordova.exec(success_callback, error_callback, "kioskmanager", "setPinnedMode", [state]);
  },
  removeDeviceOwner: function(success_callback, error_callback) {
    cordova.exec(success_callback, error_callback, "kioskmanager", "removeDeviceOwner", []);
  },
  setSleepButton: function(state, success_callback, error_callback) {
    cordova.exec(success_callback, error_callback, "kioskmanager", "setSleepButton", [state]);
  },
  setPowerButton: function(state, success_callback, error_callback) {
    cordova.exec(success_callback, error_callback, "kioskmanager", "setPowerButton", [state]);
  },
  wakeupDevice: function(success_callback, error_callback) {
    cordova.exec(success_callback, error_callback, "kioskmanager", "wakeupDevice", []);
  },
  isScreenOn: function(success_callback, error_callback) {
    cordova.exec(success_callback, error_callback, "kioskmanager", "isScreenOn", []);
  },
};
