## Device Owner Provisioning

__Wichtig__: 

1. Den Device Owner kann man anscheinend nur nach einem Factory Reset setzen. D.h. wenn ihr den Owner auf eine andere App setzen wollt, müsst ihr das Gerät "resetten". Am einfachsten ist das Testen mit einem Emulator, da reicht ein "Wipe".

2. Die App muss auch bereits am Device installiert sein damit ihr das Shell Command ausführen könnt.

3. Es darf noch keine Google Account am Device aktiviert sein wenn der Device Owner gesetzt wird

```
adb shell "dpm set-device-owner <APP_PACKAGE_NAME>/plugin.kioskmanager.KioskManagerAdminReceiver"
```

z.B. für die Testapp:

```
adb shell "dpm set-device-owner io.cordova.hellocordova/plugin.kioskmanager.KioskManagerAdminReceiver"
```

## Device Owner Entfernen

1. Via Factory Reset am Device, oder "Wipe Data" im Emulator

2. Via `Kioskmanager.removeDeviceOwner` Call

3. Settings -> Location and Security -> Device Administrator

Variante 2 und 3 funktionieren leider nicht auf jedem Device.

## AndroidManifest Configuration

In der AndroidManifest.xml müssen die Categories `android.intent.category.HOME` und `android.intent.category.DEFAULT` bei der Cordova MainActivity eingetragen werden, d.h. aus:

```
<activity android:name=".MainActivity" ...>
    <intent-filter>
        <action android:name="android.intent.action.MAIN" />
        <category android:name="android.intent.category.LAUNCHER" />
    </intent-filter>
</activity>
```

wird:

```
<activity android:name=".MainActivity" ...>
    <intent-filter>
        <action android:name="android.intent.action.MAIN" />
        <category android:name="android.intent.category.LAUNCHER" />
        <category android:name="android.intent.category.HOME" />
        <category android:name="android.intent.category.DEFAULT" />
    </intent-filter>
</activity>
```

## API Calls

### App als Home Activity setzen/zurücksetzen

```
Kioskmanager.setHomeActivity(true, function() { console.log("success")}, function(e) { console.log("error: ", e)});
```

```
Kioskmanager.setHomeActivity(false, function() { console.log("success")}, function(e) { console.log("error: ", e)});
```

### Pinned Mode aktivieren/deaktivieren

```
Kioskmanager.setPinnedMode(true, function() { console.log("success")}, function(e) { console.log("error: ", e)});
```

```
Kioskmanager.setPinnedMode(false, function() { console.log("success")}, function(e) { console.log("error: ", e)});
```

### Sleep Button aktivieren/deaktivieren

```
Kioskmanager.setSleepButton(true, function() { console.log("success")}, function(e) { console.log("error: ", e)});
```

```
Kioskmanager.setSleepButton(false, function() { console.log("success")}, function(e) { console.log("error: ", e)});
```

### Power Off Button/Dialog aktivieren/deaktivieren

```
Kioskmanager.setPowerButton(true, function() { console.log("success")}, function(e) { console.log("error: ", e)});
```

```
Kioskmanager.setPowerButton(false, function() { console.log("success")}, function(e) { console.log("error: ", e)});
```

### Abfragen ob der Screen aktiv ist

```
Kioskmanager.isScreenOn(function(result) { console.log("success", result.isScreenOn)}, function(e) { console.log("error: ", e)});
```

__result__ liefert über das Feld _isScreenOn_ zurück ob der Screen im Moment eingeschalten ist.

### Screen einschalten / Gerät aufwecken

```
Kioskmanager.wakeupDevice(function() { console.log("success")}, function(e) { console.log("error: ", e)});
```

### Fehler Codes

* `invalid_json` - Dem Plugin wurden ungültige parameter übergeben (Falscher type etc.)

* `security_exception` - Nicht genügend Berechtigungen für das Kommando

* `device_owner` - Der Device Owner wurde nicht korrekt gesetzt

* `unknown` - Unbekannter Fehlertyp
