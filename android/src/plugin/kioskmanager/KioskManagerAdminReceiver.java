package plugin.kioskmanager;

import android.app.admin.DeviceAdminReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

public class KioskManagerAdminReceiver extends DeviceAdminReceiver {
	@Override
	public void onEnabled(Context context, Intent intent) {}

	@Override
	public CharSequence onDisableRequested(Context context, Intent intent) { return "Warning: Device Admin is going to be disabled."; }

	@Override
	public void onDisabled(Context context, Intent intent) {}

	@Override
	public void onLockTaskModeEntering(Context context, Intent intent, String pkg) {}

	@Override
	public void onLockTaskModeExiting(Context context, Intent intent) {}

	public static ComponentName getComponentName(Context context) {
		return new ComponentName(context.getApplicationContext(), KioskManagerAdminReceiver.class);
    }
}