package plugin.kioskmanager;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.PowerManager;
import android.view.ViewTreeObserver;
import android.view.WindowManager;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.atomic.AtomicBoolean;

import static android.content.Context.POWER_SERVICE;

public class KioskManagerPlugin extends CordovaPlugin {

    // defs
    private static final String ACTION_SET_HOME_ACTIVITY = "setHomeActivity";
    private static final String ACTION_SET_PINNED_MODE = "setPinnedMode";
    private static final String ACTION_REMOVE_DEVICE_OWNER = "removeDeviceOwner";
    private static final String ACTION_SET_SLEEP_BUTTON = "setSleepButton";
    private static final String ACTION_SET_POWER_BUTTON = "setPowerButton";
    private static final String ACTION_WAKEUP_DEVICE = "wakeupDevice";
    private static final String ACTION_IS_SCREEN_ON = "isScreenOn";

    private static final String ERROR_CODE_INVALID_JSON = "invalid_json";
    private static final String ERROR_CODE_SECURITY_EXCEPTION = "security_exception";
    private static final String ERROR_CODE_DEVICE_OWNER = "device_owner";
    private static final String ERROR_CODE_UNKNOWN = "unknown";

    // state
    public static final AtomicBoolean disallowSleepMode = new AtomicBoolean(false);
    public static final AtomicBoolean disallowPowerOffDialog = new AtomicBoolean(false);

    // life-cycle

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        try {
            if(ACTION_SET_HOME_ACTIVITY.equals(action)) {
                actionSetHomeActivity(callbackContext, args);
            } else if(ACTION_SET_PINNED_MODE.equals(action)) {
                actionSetPinnedMode(callbackContext, args);
            } else if(ACTION_REMOVE_DEVICE_OWNER.equals(action)) {
                actionRemoveDeviceOwner(callbackContext);
            } else if(ACTION_SET_SLEEP_BUTTON.equals(action)) {
                actionSetSleepButton(callbackContext, args);
            } else if(ACTION_SET_POWER_BUTTON.equals(action)) {
                actionSetPowerButton(callbackContext, args);
            } else if(ACTION_WAKEUP_DEVICE.equals(action)) {
                actionWakeupDevice(callbackContext);
            } else if(ACTION_IS_SCREEN_ON.equals(action)) {
                actionIsScreenOn(callbackContext);
            }
        } catch(JSONException e) {
            callbackContext.error(error(ERROR_CODE_INVALID_JSON, e.getMessage()));
            return false;
        } catch(SecurityException e) {
            callbackContext.error(error(ERROR_CODE_SECURITY_EXCEPTION, e.getMessage()));
            return false;
        } catch(DeviceOwnerException e) {
            callbackContext.error(error(ERROR_CODE_DEVICE_OWNER, e.getMessage()));
            return false;
        } catch(Exception e) {
            callbackContext.error(error(ERROR_CODE_UNKNOWN, e.getMessage()));
            return false;
        }

        return true;
    }

    // actions
    private void actionSetHomeActivity(CallbackContext callbackContext, JSONArray args) throws JSONException, DeviceOwnerException {
        boolean enable = args.getBoolean(0);

        if(enable) {
            setPreferredLauncher(cordova.getActivity());
        } else {
            clearPreferredLauncher(cordova.getActivity());
        }

        callbackContext.success();
    }

    private void actionSetPinnedMode(CallbackContext callbackContext, JSONArray args) throws JSONException, DeviceOwnerException {
        boolean enable = args.getBoolean(0);

        if(enable) {
            enableLockTaskMode(cordova.getActivity());
        } else {
            disableLockTaskMode(cordova.getActivity());
        }
        callbackContext.success();
    }

    private void actionRemoveDeviceOwner(CallbackContext callbackContext) throws JSONException, DeviceOwnerException {
        removeDeviceOwner(cordova.getActivity());
        callbackContext.success();
    }

    private void actionSetSleepButton(CallbackContext callbackContext, JSONArray args) throws JSONException, DeviceOwnerException {
        boolean enable = args.getBoolean(0);

        if(enable) {
            enableSleepButton();
        } else {
            disableSleepButton();
        }
        callbackContext.success();
    }

    private void actionSetPowerButton(CallbackContext callbackContext, JSONArray args) throws JSONException, DeviceOwnerException {
        boolean enable = args.getBoolean(0);

        if(enable) {
            enablePowerButton();
        } else {
            disablePowerButton();
        }
        callbackContext.success();
    }

    private void actionWakeupDevice(CallbackContext callbackContext) throws JSONException, DeviceOwnerException {
        PowerManager.WakeLock wakeLock = KioskManagerApplication.class.cast(cordova.getActivity().getApplication()).getWakeLock();
        if (wakeLock.isHeld()) {
            wakeLock.release();
        }
        wakeLock.acquire();
        wakeLock.release();
        callbackContext.success();
    }

    private void actionIsScreenOn(CallbackContext callbackContext) throws JSONException, DeviceOwnerException {
        PowerManager powerManager = PowerManager.class.cast(cordova.getActivity().getSystemService(POWER_SERVICE));
        boolean isScreenOn = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH
                ? powerManager.isInteractive()
                : powerManager.isScreenOn();

        callbackContext.success(new JSONObject().put("isScreenOn", isScreenOn));
    }

    // lock task

    private static void enableLockTaskMode(Activity a) throws DeviceOwnerException {
        DevicePolicyManager dpm = (DevicePolicyManager) a.getApplicationContext().getSystemService(Context.DEVICE_POLICY_SERVICE);

        if(!dpm.isDeviceOwnerApp(a.getPackageName())) {
            throw new DeviceOwnerException();
        }

        ComponentName deviceAdmin = KioskManagerAdminReceiver.getComponentName(a);

        dpm.setLockTaskPackages(deviceAdmin, new String[] { a.getPackageName() });

        if (dpm.isLockTaskPermitted(a.getApplicationContext().getPackageName())) {
            a.startLockTask();
        }
    }

    private static void disableLockTaskMode(Activity a) throws DeviceOwnerException {
        DevicePolicyManager dpm =
                DevicePolicyManager.class.cast(a.getApplicationContext()
                        .getSystemService(Context.DEVICE_POLICY_SERVICE));

        if(!dpm.isDeviceOwnerApp(a.getPackageName())) {
            throw new DeviceOwnerException();
        }

        ActivityManager activityManager =  ActivityManager.class.cast(a.getSystemService(Context.ACTIVITY_SERVICE));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activityManager.getLockTaskModeState() == ActivityManager.LOCK_TASK_MODE_LOCKED) {
                a.stopLockTask();
            }
        } else {
            if (activityManager.isInLockTaskMode()) {
                a.stopLockTask();
            }
        }
    }

    // preferred activity

    private static void setPreferredLauncher(Activity a) throws DeviceOwnerException {
        ComponentName mainComponent = new ComponentName(a.getApplicationContext(), a.getClass());
        setPreferredLauncher(a, mainComponent);
    }

    private static void clearPreferredLauncher(Activity a) throws DeviceOwnerException {
        DevicePolicyManager dpm = (DevicePolicyManager) a.getApplicationContext().getSystemService(Context.DEVICE_POLICY_SERVICE);

        if(!dpm.isDeviceOwnerApp(a.getPackageName())) {
            throw new DeviceOwnerException();
        }

        dpm.clearPackagePersistentPreferredActivities(KioskManagerAdminReceiver.getComponentName(a.getApplicationContext()), a.getPackageName());
//        ComponentName defaultLauncher = getDefaultLauncher(a);

//        if(defaultLauncher == null) {
//            dpm.clearPackagePersistentPreferredActivities(KioskManagerAdminReceiver.getComponentName(a.getApplicationContext()), a.getPackageName());
//        } else {
//            setPreferredLauncher(a, defaultLauncher);
//        }
    }

    // device owner handling

    private static void removeDeviceOwner(Activity a) throws DeviceOwnerException {
        DevicePolicyManager dpm =
                DevicePolicyManager.class.cast(a.getApplicationContext()
                        .getSystemService(Context.DEVICE_POLICY_SERVICE));

        if(!dpm.isDeviceOwnerApp(a.getPackageName())) {
            throw new DeviceOwnerException();
        }

        dpm.removeActiveAdmin(KioskManagerAdminReceiver.getComponentName(a));
    }

    // util

    private static void setPreferredLauncher(Activity a, ComponentName componentName) throws DeviceOwnerException {
        DevicePolicyManager dpm = (DevicePolicyManager) a.getApplicationContext().getSystemService(Context.DEVICE_POLICY_SERVICE);

        if(!dpm.isDeviceOwnerApp(a.getPackageName())) {
            throw new DeviceOwnerException();
        }

        IntentFilter filter = new IntentFilter(Intent.ACTION_MAIN);
        filter.addCategory(Intent.CATEGORY_HOME);
        filter.addCategory(Intent.CATEGORY_DEFAULT);

        ComponentName deviceAdmin = KioskManagerAdminReceiver.getComponentName(a);

        dpm.addPersistentPreferredActivity(deviceAdmin, filter, componentName);
    }

//    private static ComponentName getDefaultLauncher(Activity a) {
//        PackageManager pm = a.getPackageManager();
//        Intent i = new Intent(Intent.ACTION_MAIN);
//        i.addCategory(Intent.CATEGORY_HOME);
//        List<ResolveInfo> homeActivities = pm.queryIntentActivities(i, 0);
//
//        if(homeActivities.size() == 2) {
//            for (ResolveInfo resolveInfo : homeActivities) {
//                if(resolveInfo.activityInfo.packageName.equals(a.getPackageName())) {
//                    continue;
//                }
//
//                return new ComponentName(
//                    a.getApplicationContext(), resolveInfo.activityInfo.targetActivity
//                );
//            }
//        }
//
//        return null;
//    }

    // sleep button

    private void enableSleepButton() {
        cordova.getActivity().runOnUiThread(new Runnable() {
            public void run() {
                cordova.getActivity().getWindow().clearFlags(
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
            }
        });

        disallowSleepMode.set(false);
    }

    private void disableSleepButton() {
        cordova.getActivity().runOnUiThread(new Runnable() {
            public void run() {
                cordova.getActivity().getWindow().addFlags(
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
            }
        });

        disallowSleepMode.set(true);
    }

    // power off dialog

    private final ViewTreeObserver.OnWindowFocusChangeListener windowFocusChangeListener = new ViewTreeObserver.OnWindowFocusChangeListener() {
        @Override
        public void onWindowFocusChanged(boolean hasFocus) {
            if(!hasFocus && disallowPowerOffDialog.get()) {
                Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
                cordova.getActivity().sendBroadcast(closeDialog);
            }
        }
    };

    private void enablePowerButton() {
        if(disallowPowerOffDialog.get()) {
            disallowPowerOffDialog.set(false);

            cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    webView.getView().getViewTreeObserver().removeOnWindowFocusChangeListener(windowFocusChangeListener);
                }
            });
        }
    }

    private void disablePowerButton() {
        if(!disallowPowerOffDialog.get()) {
            disallowPowerOffDialog.set(true);

            cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    webView.getView().getViewTreeObserver().addOnWindowFocusChangeListener(windowFocusChangeListener);
                }
            });
        }
    }

    // errors

    private static JSONObject error(String code, String message) {
        JSONObject error = new JSONObject();

        try {
            error.put("code", code);
            if(message!=null) {
                error.put("message", message);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return error;
    }

    private static class DeviceOwnerException extends Exception {
        DeviceOwnerException() {
            super("device admin not enabled for this activity");
        }
    }
}