package plugin.kioskmanager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

public class ScreenOffReceiver extends BroadcastReceiver {

  @Override
  public void onReceive(Context context, Intent intent) {
    if(Intent.ACTION_SCREEN_OFF.equals(intent.getAction())){
      if(KioskManagerPlugin.disallowSleepMode.get()) {
        wakeUpDevice(context);
      }
    }
  }

  private void wakeUpDevice(Context context) {
    PowerManager.WakeLock wakeLock = KioskManagerApplication.class.cast(context).getWakeLock();
    if (wakeLock.isHeld()) {
      wakeLock.release();
    }
    wakeLock.acquire();

    wakeLock.release();
  }
}
