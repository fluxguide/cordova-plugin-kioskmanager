package plugin.kioskmanager;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;

public class KioskManagerApplication extends Application {

  // deps

  private PowerManager.WakeLock wakeLock;
  private ScreenOffReceiver screenOffReceiver;

  // life-cycle

  @Override
  public void onCreate() {
    super.onCreate();
    registerKioskManagerScreenOffReceiver();
  }

  // interface

  public PowerManager.WakeLock getWakeLock() {
    if(wakeLock == null) {
      PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
      wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "kioskManagerWakelock");
    }

    return wakeLock;
  }

  // helper

  private void registerKioskManagerScreenOffReceiver() {
    final IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
    this.screenOffReceiver = new ScreenOffReceiver();
    registerReceiver(this.screenOffReceiver, filter);
  }


}
